<?php
use think\facade\Route;

Route::group('workflow', function () {
    //manage
    Route::rule('manage/lists', '\lhq\workflow\controller\Manage@lists');
    Route::rule('manage/console', '\lhq\workflow\controller\Manage@console');
    //index
    Route::rule('index/lists', '\lhq\workflow\controller\Index@lists');
    Route::rule('index/listsAjax', '\lhq\workflow\controller\Index@listsAjax');

});