<?php

/**
 * 最底层-模型
 */

namespace lhq\workflow;

use lhq\workflow\service\common\UtilTrait;
use lhq\workflow\service\model\ModelTrait;
use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

class BaseModel extends Model
{
    use ModelTrait, UtilTrait, SoftDelete;

    protected $deleteTime = 'deleted_at';
    protected $defaultSoftDelete = 0;
    protected $hidden = ['deleted_at'];

    //public $powerField = [];
    public $fieldItem = []; //字段-字典
    public $fieldDict = []; //字段的备注


    public $objWhere = null;
    public $arrWhere = [];

    /**
     * 获取主键名
     */
    public function pk()
    {
        return $this->pk;
    }


    /**
     * 获取表的字段备注
     *
     * @return array
     */
    public function fieldDict()
    {
        if (isset($this->fieldDict) && !empty($this->fieldDict)) {
            return $this->fieldDict;
        }
        $data = Db::connect($this->connection)->query("show full columns from oa_" . $this->name . ";");
        $field = $fieldItem = [];
        foreach ($data as $v) {
            $arrItem = [];
            if (empty($v['Comment'])) {
                continue;
            }
            $arrDict = explode("?", $v['Comment']);
            if (!empty($arrDict[0])) {
                $field[$v['Field']] = $this->getCenterStr($arrDict[0], "[", "]");
            }
            if (!empty($arrDict[1])) {
                $arrItem = explode("&", $arrDict[1]);
                foreach ($arrItem as $i) {
                    if (empty($i)) {
                        continue;
                    }
                    list($key, $value) = explode("=", $i);
                    $fieldItem[$v['Field']][$key] = $value;
                }
            }
        }
        $this->fieldItem = $fieldItem;
        $this->fieldDict = $field;
        return $this;
    }

    public function getParam()
    {
        return $this->param;
    }
}
