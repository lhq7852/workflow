<?php

namespace lhq\workflow\controller;

use lhq\workflow\BaseController;
use lhq\workflow\service\common\ControllerTrait;

class Manage extends BaseController
{
    use ControllerTrait;

    public function lists()
    {
        return $this->workflowView('manage/lists', get_defined_vars());
    }

    public function console()
    {
        return $this->workflowView('manage/console', get_defined_vars());
    }
}
