<?php

namespace lhq\workflow\controller;

use lhq\workflow\BaseController;
use lhq\workflow\service\common\ControllerTrait;
use lhq\workflow\service\WorkflowService;

class Index extends BaseController
{
    use ControllerTrait;

    public function lists()
    {
        return $this->workflowView('index/lists', get_defined_vars());
    }

    public function listsAjax()
    {
        try {
            $param = $this->request->param();
            $data = WorkflowService::_getDataList($param);
            return $this->success($data, false);
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }
}
