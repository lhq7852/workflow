/**
 * 自定义x6节点
 */
(function ($, X6) {
    const { Rect, Circle, Polygon, Image, Cylinder } = X6.Shape
    //定义链接桩
    const ports = {
        groups: {
            top: {
                position: 'top', attrs: {
                    circle: {
                        r: 4, magnet: true, stroke: '#D06269', strokeWidth: 1, fill: '#fff', style: {
                            visibility: 'hidden',
                        },
                    },
                },
            }, right: {
                position: 'right', attrs: {
                    circle: {
                        r: 4, magnet: true, stroke: '#D06269', strokeWidth: 1, fill: '#fff', style: {
                            visibility: 'hidden',
                        },
                    },
                },
            }, bottom: {
                position: 'bottom', attrs: {
                    circle: {
                        r: 4, magnet: true, stroke: '#D06269', strokeWidth: 1, fill: '#fff', style: {
                            visibility: 'hidden',
                        },
                    },
                },
            }, left: {
                position: 'left', attrs: {
                    circle: {
                        r: 4, magnet: true, stroke: '#D06269', strokeWidth: 1, fill: '#fff', style: {
                            visibility: 'hidden',
                        },
                    },
                },
            },
        }, items: [{
            group: 'top', id: 't1'
        }, {
            group: 'right',
            id: 'r1'
        },
        {
            group: 'bottom', id: 'b1'
        }, {
            group: 'left',
            id: 'l1'
        }],
    }
    // 开始节点
    const start_data = {
        inherit: 'rect', width: 80, height: 45, attrs: {
            rect: { fill: '#fff', stroke: 'rgb(37,147,15)', strokeWidth: 2, rx: 15, ry: 15 },
            text: { text: '开始', fill: 'black', fontSize: 13 },
        }, data: 'node-start', ports: {
            ...ports, items: [{
                group: 'bottom', id: 'b1'
            }]
        },
    }
    const startNode = new Rect(start_data)
    X6.Graph.registerNode('node-start', start_data);

    //网关节点
    const gateway_node = {
        inherit: 'polygon', width: 65, height: 65, label: '网关', attrs: {
            body: {
                fill: '#FFF', stroke: 'rgb(22,143,168)', refPoints: '0,10 10,0 20,10 10,20',
            },
        }, data: 'node-gateway', ports: { ...ports }
    }
    const judgeNode = new Polygon(gateway_node)
    X6.Graph.registerNode('node-gateway', gateway_node);

    //结束节点
    const end_data = {
        inherit: 'rect', width: 80, height: 45, attrs: {
            rect: { fill: '#fff', stroke: 'rgb(168,18,17)', strokeWidth: 2, rx: 15, ry: 15 },
            text: { text: '结束', fill: 'black', fontSize: 13 },
        }, data: 'node-end', ports: { ...ports }
    }
    const endNode = new Rect(end_data)
    X6.Graph.registerNode('node-end', end_data);

    $.extend({ endNode, judgeNode, startNode });
})($, X6);