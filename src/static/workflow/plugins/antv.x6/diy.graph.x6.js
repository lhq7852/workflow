/**
 * 自定义x6画布
 */
(function ($, X6) {

    var mygraph = function (graphId, config) {

        graph = new X6.Graph({
            container: document.getElementById(graphId),

            connecting: {
                router: 'manhattan',
                connector: {
                    name: 'rounded',
                    args: {
                        radius: 8,
                    },
                },
                anchor: 'center',
                connectionPoint: 'anchor',
                allowBlank: false,
                snap: true,
                createEdge() {
                    return new X6.Shape.Edge({
                        attrs: {
                            line: {
                                stroke: '#A2B1C3',
                                strokeWidth: 2,
                                targetMarker: {
                                    name: 'block',
                                    width: 12,
                                    height: 8,
                                },
                            },
                        },
                        zIndex: 0,
                    })
                },
                validateConnection({ targetMagnet }) {
                    return !!targetMagnet
                }
            },
            resizing: {
                enabled: true,
            },
            rotating: {
                enabled: true,
            },
            panning: {
                enabled: true,
                modifiers: 'shift',
            },
            selecting: {
                modifiers: 'ctrl',
                enabled: true,
                multiple: true,
                rubberband: true, // 启用框选
                showNodeSelectionBox: true,
            },
            width: "100%",
            height: "100%",
            snapline: true,//对齐线
            // background: {
            //     color: '#fffbe6', // 设置画布背景颜色
            // },
            grid: {
                size: 10,      // 网格大小 10px
                visible: true, // 渲染网格背景
            },
            keyboard: true,//绑定快捷键盘
            //剪切板
            clipboard: {
                enabled: true,
                useLocalStorage: true,
            },
            history: true,//撤销/重做能力
        });
        // graph.fromJSON(data)

        graph.translate(180, 140)
        graph.centerContent()
        //鼠标移上去
        graph.on('node:mouseenter', ({ e, cell, view }) => {
            //删除工具
            // cell.addTools({
            //     name: 'button-remove',
            //     args: {
            //         x: 0,
            //         y: 0,
            //         offset: { x: 10, y: 10 },
            //     },
            // })
            $.each($("[data-cell-id=" + cell.id + "]").find(".x6-port-body"), function (k, v) {
                $(this).css("visibility", "visible")
            })
        })
        //鼠标离开
        graph.on('node:mouseleave', ({ e, cell, view }) => {
            //删除工具
            // cell.removeTools({
            //     name: 'button-remove',
            //     args: {
            //         x: 0,
            //         y: 0,
            //         offset: { x: 10, y: 10 },
            //     },
            // })
            $.each($("[data-cell-id=" + cell.id + "]").find(".x6-port-body"), function (k, v) {
                $(this).css("visibility", "hidden")
            })
        })
        //双击事件
        graph.on('node:dblclick', ({ e, cell, view }) => {
            if (typeof config.dblclick == "function") {
                var editTxt = function (txt) {
                    $("[data-cell-id=" + cell.id + "]").find(".v-line").text(txt);
                }
                config.dblclick({ "id": cell.id, editTxt });
            }
        })
        //绑定事件
        window.onkeydown = function (e) {
            var keyCode = e.keyCode || e.which || e.charCode;
            var ctrlKey = e.ctrlKey || e.metaKey;
            if (ctrlKey && keyCode == 83) {
                e.preventDefault();
                if (typeof config.saveFunc === "function") {
                    config.saveFunc(graph.toJSON())
                }
                return false;
            }
        }
        graph.bindKey(['ctrl+s'], () => {
            if (typeof config.saveFunc === "function") {
                config.saveFunc(graph.toJSON())
            }
            return false
        })
        //撤回
        graph.bindKey(['ctrl+z'], () => {
            graph.history.undo()
            return false
        })
        //重做
        graph.bindKey(['ctrl+y'], () => {
            graph.history.redo()
            return false
        })
        //删除
        graph.bindKey(['delete', 'backspace'], () => {
            const cells = graph.getSelectedCells()
            if (cells.length) {
                graph.cut(cells)
                graph.cleanClipboard()
            }
            return false
        })
        //复制
        graph.bindKey(['meta+c', 'ctrl+c'], () => {
            const cells = graph.getSelectedCells()
            if (cells.length) {
                graph.copy(cells)
            }
            return false
        })
        //剪切
        graph.bindKey(['meta+x', 'ctrl+x'], () => {
            const cells = graph.getSelectedCells()
            if (cells.length) {
                graph.cut(cells)
            }
            return false
        })
        //粘贴
        graph.bindKey(['meta+v', 'ctrl+v'], () => {
            if (!graph.isClipboardEmpty()) {
                const cells = graph.paste({ offset: 32 })
                graph.cleanSelection()
                graph.select(cells)
            }
            return false
        })

        //--工具栏外置按钮操作-----------Start------------
        //清空画布
        if (typeof config.clearAllBtn != "undefined") {
            $(config.clearAllBtn).click(() => {
                graph.clearCells()
            })
        }
        if (typeof config.saveBtn != "undefined" && typeof config.saveFunc === "function") {
            $(config.saveBtn).click(() => {
                config.saveFunc(graph.toJSON())
            })
        }
        // --工具栏外置按钮操作-----------End------------
        //返回画布对象
        return graph
    }

    $.extend({ mygraph });
})($, X6);