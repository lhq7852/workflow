/**
 * 自定义工具栏
 */

(function ($, X6) {
    //自定义工作栏
    var mystencil = function (sidebarId) {
        const { Stencil } = X6.Addon
        const stencil = new Stencil({
            title: '流程工具栏',
            target: graph,
            stencilGraphWidth: 110,
            stencilGraphHeight: document.body.offsetHeight - 125,
            layoutOptions: {
                columns: 1,
                columnWidth: 80,
                rowHeight: 80,
                marginY: 20,
            }
        })
        const stencilContainer = document.getElementById(sidebarId)
        // 渲染dom--
        stencilContainer.appendChild(stencil.container)
        return stencil;
    }

    $.extend({ mystencil });
})($, X6);