﻿var myCalendar = null;

$(function () {
    var height = window.screen.height * 0.7405;

    myCalendar = new SimpleCalendar('#container', {
        height: height + 'px',
    });
    LoadRiChengData(myCalendar);


    layui.use(["layer", 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.$,
            laydate = layui.laydate;
        var date = new Date();
        var currDay = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        SearchRiCheng(currDay);


        $("#addDiv").click(function () {
            var _index = layer.open({
                type: 1,
                shade: false,
                title: '新增日程安排', //不显示标题
                area: ["60%", "75%"],
                content: $('#addOpenDiv'),
                btn: ['保存', '关闭'],
                yes: function () {
                    //此处请根据需要连接后台平台
                    var currentDay = $("#riqi").val();
                    var title = $("#title").val();
                    var content = $("#content").val();
                    //if (Ext.isEmpty(currentDay)) {
                    //    tool.ShowMessagePopup(2, "日期不允许为空");
                    //    return;
                    //}
                    //else if (Ext.isEmpty(title)) {
                    //    tool.ShowMessagePopup(2, "标题不允许为空");
                    //    return;
                    //}
                    //else if (Ext.isEmpty(content)) {
                    //    tool.ShowMessagePopup(2, "日程安排不允许为空");
                    //    return;
                    //}

                    layer.msg("保存成功", { time: 2000 }, function (index) {
                        myCalendar.addMark(currentDay, '日程');
                        SearchRiCheng(currentDay);
                        $("#addOpenDiv").hide();
                    });
                },
                btn2: function (index, layero) {
                    layer.close(index);
                    $("#addOpenDiv").hide();
                },
                success: function () {
                    $("#riqi").val('');
                    $("#title").val('');
                    $("#content").val('');

                    laydate.render({
                        elem: '#riqi',
                        format: 'yyyy-M-d',
                        value: $("#currentDaySpan").text()
                    });
                },
                cancel: function () {
                    $("#addOpenDiv").hide();
                },
                end: function () {
                    $("#addOpenDiv").hide();
                }
            });
        })
    });
})


function SearchRiCheng(day) {
    //此处需要与后台关联，请自行处理。。。
    $("#currentDaySpan").text(day);
    var url = '/rostering.rostering/getDataDayAjax?date='+day;
    $.get(url, function (ret) {
        $.each(ret.data, function(index, value) {
            $("#"+value.rostering_business+"_"+value.getTimeSlot+"_"+value.rosteringType).val(value.rostering_admin_id);
        });
    }, "json");
}

//查看某个日程的明细
function SearchDetailRC(id) {
    layui.use(["layer", 'laydate', 'form'], function () {
        var layer = layui.layer,
            $ = layui.$,
            laydate = layui.laydate,
            form = layui.form;

        layer.open({
            type: 1,
            shade: false,
            title: '日程安排详情',
            area: ["60%", "75%"],
            content: $('#addOpenDiv'),
            btn: ['保存', '完结', '关闭'],
            yes: function (index, layero) {
                var currentDay = $("#riqi").val();
                var title = $("#title").val();
                var content = $("#content").val();
                if (Ext.isEmpty(currentDay)) {
                    tool.ShowMessageLayer("日期不允许为空");
                    return;
                }
                else if (Ext.isEmpty(title)) {
                    tool.ShowMessageLayer("标题不允许为空");
                    return;
                }
                else if (Ext.isEmpty(content)) {
                    tool.ShowMessageLayer("日程安排不允许为空");
                    return;
                }

                layer.msg("用于展示详情", { time: 2000 }, function (index) {
                    myCalendar.addMark(currentDay, '日程');
                    SearchRiCheng(currentDay);
                    $("#addOpenDiv").hide();
                });
            },
            btn2: function (index, layero) {
                //完结按钮操作逻辑

                //var _day = $("#riqi").val();
                //WanJie(id, _day);
                //layer.close(index);
                return false;//此处不可屏蔽
            },
            btn3: function (index, layero) {
                layer.close(index);
                $("#addOpenDiv").hide();
            },
            success: function (index, layero) {
                //弹窗展示后，回填数据到对应的文本框中

                //ajax.ExecuteMethod({ ns: ns, cn: cn, ay: ay, method: "GetDetailRiCheng", arg: id }, function (obj) {
                //    if (obj) {
                //        if (obj.ResultObj.length > 0) {
                //            Ext.each(obj.ResultObj, function (item, index) {
                //                $('#riqi').val(tool.FormatDate(item.VoucherDate, 7));
                //                $('#title').val(item.Title);
                //                $('#content').val(item.Content);
                //                if (item.IsWanJie == 1) {
                //                    $("#wanjie").prop("checked", true);
                //                }
                //                else
                //                    $("#wanjie").prop("checked", false);
                //            });
                //            form.render();
                //        }
                //    }
                //});
            },
            end: function () {
                $("#addOpenDiv").hide();
            }
        });
    });
}

//获取含有日程的数据
function LoadRiChengData(myCalendar) {
    //获取有日程安排的数据，并打上日程标记，同时更新日历

    //ajax.ExecuteMethod({ ns: ns, cn: cn, ay: ay, method: "GetRiChengData", arg: '' }, function (obj) {
    //    if (obj) {
    //        if (obj.ResultObj.length > 0) {
    //            Ext.each(obj.ResultObj, function (item, index) {
    //                myCalendar.addMark(item, '日程');//打上日程标记
    //            });
    //            //更新日历
    //            myCalendar.update();
    //        }
    //    }
    //});
}

//删除日程
function DeleteDetailRC(id, voucherdate) {
    layui.use(["layer"], function () {
        var layer = layui.layer;
        layer.confirm("确定删除吗？", {
            //skin: 'demoClass',
            closeBtn: 2,
            icon: 3,
            title: '提示',
            btn: ['确定', '取消'] //按钮
        }, function (index) {
            //确定按钮操作逻辑。。。。
            layer.msg("操作成功");

            //根据后台返回结果是否显示“日程”标记
            //if (obj.ResultObj.indexOf("去除标记") > -1) {
            //    myCalendar.addMark(day, '');
            //}
            //else
            //    myCalendar.addMark(day, '日程');


            SearchRiCheng(voucherdate);
            layer.close(index);
        }, function (index) {
            //取消按钮操作逻辑
            layer.close(index);
        });
    });
}

//完结
function WanJie(id, voucherdate) {
    layui.use(["layer"], function () {
        var layer = layui.layer;
        layer.confirm("确定完结吗？", {
            //skin: 'demoClass',
            closeBtn: 2,
            icon: 3,
            title: '提示',
            btn: ['确定', '取消'] //按钮
        }, function (index) {
            //确定按钮操作逻辑。。。。
            layer.msg("操作成功");
            SearchRiCheng(voucherdate);
            layer.close(index);
        }, function (index) {
            //取消按钮操作逻辑
            layer.close(index);
        });
    });
}
