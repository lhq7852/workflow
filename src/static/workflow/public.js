layui.define(['jquery', 'layer'], function (exports) { //提示：模块也可以依赖其它模块，如：layui.define('mod1', callback);

    var $ = layui.jquery;
    /**
     * 表单回调处理
     */
    var formBack = function (arg = {}) {
        layer.closeAll('loading');
        if (arg.data.token) {
            $("[name=__token__]").val(arg.data.token);
        }
        if (arg.data.code != 200) {
            return error({
                'msg': arg.data.msg,
                "callBack": function () {
                    if (typeof arg.callBack == 'function') {
                        arg.callBack(arg);
                    }
                }
            });
        }
        layer.closeAll();
        success({
            "msg": arg.data.msg,
            "callBack": function () {
                if (typeof arg.callBack == 'function') {
                    arg.callBack(arg);
                }
                if (arg.reload) {
                    window.location.reload();
                }
                if (arg.url) {
                    window.location.href = arg.url;
                }
            }
        });
    }
    //成功输出
    var success = function (arg = {}) {
        layer.closeAll('loading');
        var message = arg.msg ? arg.msg : '成功';
        if (arg.token) {
            $("[name=__token__]").val(arg.token);
        }
        if (arg.callBack) {
            layer.msg(message, { icon: 1, time: 1000 }, arg.callBack);
        } else {
            layer.msg(message, { icon: 1, time: 1000 });
        }
    }
    //失败输出
    var error = function (arg = {}) {
        layer.closeAll('loading');
        var message = arg.msg ? arg.msg : '失败';
        if (arg.token) {
            $("[name=__token__]").val(arg.token);
        }
        if (arg.callBack) {
            layer.msg(message, { icon: 2 }, arg.callBack);
        } else {
            layer.msg(message, { icon: 2 });
        }
    }
    //获取令牌
    var formToken = function (arg = {}) {
        // console.log(typeof arg[0] == 'undefined');
        if (typeof arg[0] == 'object') {
            arg.push({ "name": '__token__', value: $("[name=__token__]").val() });
        } else {
            arg['__token__'] = $("[name=__token__]").val();
        }
        return arg;
    }
    //处理name,value的列表数据
    var arrSplit = function (arg = {}) {
        if (!arg.field) {
            return '';
        }
        var str = '';
        $.each(arg.data, function (k, v) {
            str += v.value + ',';
        });
        return {
            'name': arg.field,
            'value': str
        };
    }
    var formatForm = function (arg = {}) {
        var arr = {};
        $.each(arg, function (k, v) {
            arr[v.name] = v.value;
        });
        // arr['power_referer_url'] = window.location.href;
        return arr;
    }
    var copyContent = function (content) {
        var oInput = document.createElement('input');
        oInput.value = content;
        document.body.appendChild(oInput);
        oInput.select(); // 选择对象
        document.execCommand("Copy"); // 执行浏览器复制命令
        oInput.className = 'oInput';//设置class名
        document.getElementsByClassName("oInput")[0].remove();//移除这个input
        layer.msg('复制成功！', { icon: 1, time: 3000 });
    };
    //数据表格添加右键菜单
    var setmenu = function (obj = {}, pk = {}, menuItem = {}, callback = function (key, options) {
    }) {
        $('.layui-table-main tr').each(function (i) {
            var that = $(this);
            $.each(pk, function (index, value) {
                that.attr('data-' + index, obj[i][value]);
                that.addClass('addmenus');
            });
        })
        $.contextMenu('destroy');//销毁右键菜单
        $.contextMenu({
            selector: '.addmenus',
            items: menuItem,
            callback: callback
        });
    }
    var uploadInit = function (id, jsconfig, callBack) {
        var uploadOpenIndex;
        $(document).delegate('#' + id, 'click', () => {
            uploadOpenIndex = layer.open({
                type: 1,
                area: [jsconfig.open_area_lm, jsconfig.open_area_lm],
                content: $('#box-' + id),
            });
        })
        var imgArr = [];
        var fileArr = [];
        $(document).delegate('#box-' + id + ' .close-upload-box-btn', 'click', () => {
            if (typeof jsconfig.reload != 'undefined') {
                imgArr = [];
                fileArr = [];
            }
            //已上传
            $('#box-' + id + ' .uploaded-container input[type=checkbox]:checked').each(function (i, v) {
                imgArr[$(v).data('id')] = {
                    file_id: $(v).data('id'),
                    file_name: $(v).data('name'),
                    file_ext: $(v).data('ext'),
                    file_url: $(v).data('src')
                }
            });
            //刚上传
            $.each($(document).find('#box-' + id + ' .uploaded-file-id'), (i, v) => {
                imgArr[$(v).data('id')] = {
                    file_id: $(v).data('id'),
                    file_name: $(v).data('name'),
                    file_ext: $(v).data('ext'),
                    file_url: $(v).data('src')
                }
            })
            $.each(imgArr, (i, v) => {
                if (typeof v != 'undefined') {
                    fileArr.push(v)
                }
            })
            console.log(fileArr)
            if (typeof callBack == 'function') {
                callBack(fileArr);
            }
            layer.close(uploadOpenIndex);
        })
    }
    var formatUrlParams = function (arg = {}) {
        var str = "?";
        $.each(arg, function (k, v) {
            var symbol = "&"
            if (k == 0) {
                symbol = ""
            }
            str += symbol + v.name + "=" + v.value;
        });
        return str;
    }
    //禁止回退
    var closeBackspace = function(){
        window.history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            window.history.pushState(null, null, document.URL)
        }, false);
    }
    //输出 mymod 接口
    exports('public', {
        formBack, formToken, success, error, arrSplit, formatForm, copyContent, setmenu, uploadInit, formatUrlParams, closeBackspace
    });
});
