// +----------------------------------------------------------------------
// | FunAdmin极速开发框架 [基于layui开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2030 http://www.funadmin.com
// +----------------------------------------------------------------------
// | git://github.com/funadmin/funadmin.git 994927909
// +----------------------------------------------------------------------
// | Author: yuege <994927909@qq.com> Apache 2.0 License Code

// var BASE_URL = document.scripts[document.scripts.length - 1].src.substring(0, document.scripts[document.scripts.length - 1].src.lastIndexOf('/')+1);
BASE_URL = "/"
require.config({
// console.log({
    urlArgs: 'v=1',// + new Date().getTime(),
    packages: [
        {
            name: 'dayjs',
            location: '/static/plugins/dayjs',
            main: 'dayjs.min'
        }
    ],
    baseUrl: BASE_URL,
    include: [
        'css','treeGrid','tableSelect','treeTable','tableEdit','tableFilter',
        'tableTree','iconPicker','iconFonts', 'toastr','step-lay','inputTags',
        'timeago','multiSelect','cityPicker', 'selectPlus','selectN', 'xmSelect',
        'regionCheckBox','timePicker','croppers',
        'backend','md5','fun','fu','form','table','upload','addons'],
    paths: {
        'lang'          : 'empty:',
        'jquery'        : '/static/plugins/jquery/jquery-3.5.1.min', // jquery
        //layui等组件
        // 'cardTable'     : 'plugins/lay-module/cardTable/cardTable',
        'tableFilter'   : '/static/plugins/lay-module/tableFilter/tableFilter',
        'treeGrid'      : '/static/plugins/lay-module/treeGrid/treeGrid',
        'tableSelect'   : '/static/plugins/lay-module/tableSelect/tableSelect',
        'treeTable'     : '/static/plugins/lay-module/treeTable/treeTable',
        'tableEdit'     : '/static/plugins/lay-module/tableTree/tableEdit',
        'tableTree'     : '/static/plugins/lay-module/tableTree/tableTree',
        'iconPicker'    : '/static/plugins/lay-module/iconPicker/iconPicker',
        'iconFonts'     : '/static/plugins/lay-module/iconPicker/iconFonts',
        'toastr'        : '/static/plugins/lay-module/toastr/toastr',//提示框
        'step-lay'      : '/static/plugins/lay-module/step-lay/step',
        'inputTags'     : '/static/plugins/lay-module/inputTags/inputTags',
        'timeago'       : '/static/plugins/lay-module/timeago/timeago',
        'multiSelect'   : '/static/plugins/lay-module/multiSelect/multiSelect',
        'selectPlus'    : '/static/plugins/lay-module/selectPlus/selectPlus',
        'selectN'       : '/static/plugins/lay-module/selectPlus/selectN',
        'cityPicker'    : '/static/plugins/lay-module/cityPicker/city-picker',
        'regionCheckBox': '/static/plugins/lay-module/regionCheckBox/regionCheckBox',
        'timePicker'    : '/static/plugins/lay-module/timePicker/timePicker',
        'croppers'      : '/static/plugins/lay-module/cropper/croppers',
        'xmSelect'      : '/static/plugins/lay-module/xm-select/xm-select',
        'backend'       : '/static/plugins/lay-module/fun/backend.min', // fun后台扩展
        'md5'           : '/static/plugins/lay-module/md5/md5.min', // 后台扩展
        'fun'           : '/static/js/fun', // api扩展
        'fu'            : '/static/js/require-fu',
        'table'         : '/static/js/require-table',
        'form'          : '/static/js/require-form',
        'upload'        : '/static/js/require-upload',
        'addons'        : '/static/js/require-addons',//编辑器以及其他安装的插件
    },
    map: {
        '*': {'css': 'plugins/require-css/css.min'}
    },
    shim: {
        // 'cardTable':{
        //     deps: ['css!plugins/lay-module/cardTable/cardTable.css'],
        // },
        'cityPicker':{
            deps: ['plugins/lay-module/cityPicker/city-picker-data',
                'css!plugins/lay-module/cityPicker/city-picker.css'],
        },
        'tableFilter':{
            deps: ['css!plugins/lay-module/tableFilter/tableFilter.css'],
        },
        'inputTags':{
            deps: ['css!plugins/lay-module/inputTags/inputTags.css'],
        },
        'regionCheckBox':{
            deps: ['css!plugins/lay-module/regionCheckBox/regionCheckBox.css'],
        },
        'multiSelect': {
            deps: ['css!plugins/lay-module/multiSelect/multiSelect.css'],
        },
        'timePicker':{
            deps:['css!plugins/lay-module/timePicker/timePicker.css'],
        },
        'step': {
            deps: ['css!plugins/lay-module/step/step.css'],
        },
        'croppers': {
            deps: ['plugins/lay-module/cropper/cropper', 'css!plugins/lay-module/cropper/cropper.css'], exports: "cropper"
        },
    },
    waitSeconds: 30,
    charset: 'utf-8' // 文件编码
});

//初始化控制器对应的JS自动加载
require(["jquery"], function ($) {
    // 配置语言包的路径
    var paths = {};
    // paths["lang"] = '/index.php/ajax/lang?callback=define&addons='+Config.addonname+'&controllername=' + Config.controllername;
    paths["lang"] = '/center.center/lang?v=1';
    paths['backend/'] = '/backend/';

    require.config({paths:paths});
    //直接使用$经常出现未定义
    $ = layui.jquery;
    $(function () {
        require(['fun','backend','addons'], function (Fun,Backend) {
            $(function () {
                if ('undefined' != typeof Config.autojs && Config.autojs) {
                    require([BASE_URL+Config.jspath], function (Controller) {
                        if (typeof Controller!=undefined && Controller.hasOwnProperty(Config.actionname)) {
                            Controller[Config.actionname]();
                        } else {
                            console.log('action'+ Config.actionname+' is not find')
                        }
                    });
                }
            })
        })
    })
});