define(['table','form'], function (Table,Form) {
    let Controller = {
        index: function () {
            Table.init = {
                table_elem: 'list',
                tableId: 'list',
                requests:{
                    index_url:'countrycity.countryCity/index',
                    add_url:'countrycity.countryCity/add',
                    edit_url:'countrycity.countryCity/edit',
                    destroy_url:'countrycity.countryCity/destroy',

                    recycle_url:'countrycity.countryCity/recycle',
                    import_url:'countrycity.countryCity/import',
                    export_url:'countrycity.countryCity/export',
                    modify_url:'countrycity.countryCity/modify',

                }
            }
            Table.render({
                elem: '#' + Table.init.table_elem,
                id: Table.init.tableId,
                url: Fun.url(Table.init.requests.index_url),
                init: Table.init,
                toolbar: ['refresh','add','destroy','export','recycle'],
                cols: [[
                    {checkbox: true,},
                    {field: 'id', title: __('ID'), sort:true,},
                    {field:'pid',title: __('Pid'),align: 'center'},
                    {field:'nameCh', title: __('NameCh'),align: 'center'},
                    {field:'nameEn', title: __('NameEn'),align: 'center'},
                    {field:'theFirst', title: __('TheFirst'),align: 'center'},
                    {field:'sort',title: __('Sort'),align: 'center',edit:'text'},
                    {field:'level',title: __('Level'),align: 'center'},
                    {field:'operationId',title: __('OperationId'),align: 'center'},

                    {
                        minWidth: 250,
                        align: "center",
                        title: __("Operat"),
                        init: Table.init,
                        templet: Table.templet.operat,
                        operat:["edit","delete"]
                    },
                ]],
                limits: [10, 15, 20, 25, 50, 100,500],
                limit: 15,
                page: true,
                done: function (res, curr, count) {
                }
            });

            let table = $('#'+Table.init.table_elem);
            Table.api.bindEvent(table);
        },
        add: function () {
            Controller.api.bindevent()
        },
        edit: function () {
            Controller.api.bindevent()
        },
        
        api: {
            bindevent: function () {
                Form.api.bindEvent($('form'))
            }
        }
    };
    return Controller;
});