<?php

namespace lhq\workflow\service;

use lhq\workflow\service\common\FacadeTrait;
use lhq\workflow\service\common\ServiceTrait;

class WorkflowService
{
    use FacadeTrait, ServiceTrait;

}