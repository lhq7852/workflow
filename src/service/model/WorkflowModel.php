<?php

namespace lhq\workflow\service\model;


use lhq\workflow\BaseModel;

class WorkflowModel extends BaseModel
{
    protected $name = 'workflow';
    protected $pk = 'workflow_id';
    protected $alias = 'w';
}
