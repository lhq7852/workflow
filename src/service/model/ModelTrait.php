<?php

/**
 * 模型实体
 */

namespace lhq\workflow\service\model;

use lhq\workflow\service\common\ExceptionTrait;
use think\facade\Db;

trait ModelTrait
{

    use ExceptionTrait;

    protected $param = []; //传入参数
    protected $depth = 1; //传入参数的深度 1 一维 2 二维


    /**
     * 权限
     */
    public function setPower(&$objWhere, $param = [])
    {
        $param = !empty($param) ? $param : (empty($this->powerField) ? [] : $this->powerField);
        if (!empty(request()->admin['seeAdminIds']) && !empty($param)) {
            foreach ($param as $field) {
                $objWhere = $objWhere->whereIn($field, request()->admin['seeAdminIds']);
            }
        }
        return $objWhere;
    }

    /**
     * 设置传入的参数
     *
     * @param [type] $param
     * @return object
     */
    public function setParam($param = [])
    {
        $this->param = $param;
        $this->depth = $this->checkArrayDepth($param);
        return $this;
    }

    /**
     * 获取单条数据（根据键名，默认主键）
     * @param $value
     * @param array $key 如果是数组，则key不可用。
     * @return array
     */
    public function getDataInfo($param = [], $field = "*")
    {
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (empty($where)) {
            return [];
        }
        $data = $this->alias($this->alias)->where($where)->field($field)->find();
        return $data ? $data->toArray() : [];
    }

    /**
     * 获取已软删的单条数据（根据键名，默认主键）
     * @param $value
     * @param array $key 如果是数组，则key不可用。
     * @return array
     */
    public function getDeletedDataInfo($param = [], $field = "*")
    {
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (empty($where)) {
            return [];
        }
        $data = $this->withTrashed()->alias($this->alias)->where('deleted_at', '>', 0)->where($where)->field($field)->find();
        return $data ? $data->toArray() : [];
    }

    /**
     * 获取数据列表,带分页
     *
     * @param array $param
     * @return array
     */
    public function getPageList($param = [])
    {
        $objWhere = $this->setWhereLike($param);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (isset($this->arrWhere)) {
            $objWhere = $objWhere->where($this->arrWhere);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("created_at", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        //数据权限
        $objWhere = $this->setPower($objWhere, []);
        $data = $objWhere->alias($this->alias)->paginate(request()->pageSize ?? 30)->toArray();
        return $data;
    }

    /**
     * 获取数据列表,无分页
     *
     * @param array $param
     * @return array
     */
    public function getDataList($param = [])
    {
        $objWhere = $this->setWhereLike($param);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (isset($this->arrWhere)) {
            $objWhere = $objWhere->where($this->arrWhere);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("created_at", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        //数据权限
        $objWhere = $this->setPower($objWhere, []);
        $data = $objWhere->alias($this->alias)->select()->toArray();
        return $data;
    }

    /**
     * 获取软删数据列表,无分页
     *
     * @param array $param
     * @return array
     */
    public function getDataDeletedList($param = [])
    {
        $objWhere = $this->setDeletedWhereLike($param);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (isset($this->arrWhere)) {
            $objWhere = $objWhere->where($this->arrWhere);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("created_at", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        //数据权限
        $objWhere = $this->setPower($objWhere, []);
        $data = $objWhere->alias($this->alias)->where("deleted_at", ">", 0)->select()->toArray();
        return $data;
    }

    /**
     * 获取回收站数据分页列表
     *
     * @param array $param
     * @return array
     */
    public function getPageDeletedList($param = [])
    {
        $objWhere = $this->setDeletedWhereLike($param);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (isset($this->arrWhere)) {
            $objWhere = $objWhere->where($this->arrWhere);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("created_at", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        //数据权限
        $objWhere = $this->setPower($objWhere, []);
        $data = $objWhere->alias($this->alias)->where("deleted_at", ">", 0)->paginate(request()->pageSize ?? 30)->toArray();
        return $data;
    }

    /**
     * 过滤模型关联表不存在字段，并且按=条件组装where数组
     * @param array $array
     * @param array $where
     * @return array
     */
    public function filterSetWhere(array $array = [], $default = true)
    {
        if (empty($array)) {
            return [];
        }
        $where = [];
        $table_fields = $this->fieldList();
        foreach ($array as $field => $value) {
            if (!strchr($field, "|") && !in_array($field, $table_fields)) {
                continue;
            }
            if (empty($value) && !is_numeric($value)) {
                continue;
            }
            if (empty($value) && $this->pk() == $field) {
                continue;
            }
            if (is_array($value) && !empty($value[0]) && $value[0] == $field) {
                $this->arrWhere[] = $value;
                continue;
            }
            if ($default) {
                $where[$this->alias . '.' . $field] = $array[$field];
            } else {
                $where[$field] = $array[$field];
            }
        }
        return $where;
    }

    /**
     * 设置模糊查询
     *
     * @param array $param
     * @return object
     */
    public function setWhereLike($param = [], &$objWhere = null)
    {
        $objWhere = $objWhere ?? $this;
        if (empty($param)) {
            return $objWhere;
        }
        $alias = '';
        if (!empty($this->alias)) {
            $alias = $this->alias . '.';
        }
        foreach ($param as $field => $value) {
            if (!empty($this->likeList[$field]) && !empty($value)) {
                $objWhere = $objWhere->whereLike($alias . $this->likeList[$field], "%" . $value . "%");
            }
        }
        return $objWhere;
    }

    /**
     * 设置已软删模糊查询
     *
     * @param array $param
     * @return object
     */
    public function setDeletedWhereLike($param = [], &$objWhere = null)
    {
        $objWhere = $objWhere ?? $this;
        $objWhere = $objWhere->withTrashed();
        if (empty($param)) {
            return $objWhere;
        }
        $alias = '';
        if (!empty($this->alias)) {
            $alias = $this->alias . '.';
        }
        foreach ($param as $field => $value) {
            if (!empty($this->likeList[$field]) && !empty($value)) {
                $objWhere = $objWhere->whereLike($alias . $this->likeList[$field], "%" . $value . "%");
            }
        }
        return $objWhere;
    }

    /**
     * 排序
     *
     * @param [type] int $num
     * @return string
     */
    public function getOrderSort(int $num = 0)
    {
        $alias = '';
        if (!empty($this->alias)) {
            $alias = $this->alias . '.';
        }
        if (!is_numeric($num) || empty($num)) {
            if (in_array('sort', $this->fieldList())) {
                return $alias . 'sort asc';
            }
            return $alias . $this->pk() . ' desc';
        }
        $arr_order = [
            1 => $alias . 'created_at asc',
            2 => $alias . 'updated_at desc',
        ];
        if (!empty($this->orderByConfig)) { //如果有配置排序,用配置排序
            $arr_order = $this->orderByConfig;
        }
        return $arr_order[($num ?? 0)] ?? $alias . $this->pk() . ' desc';
    }

    /**
     * 新增数据
     * @param $param
     * @return mixed|null
     * @throws \Exception
     */
    public function addData()
    {
        if (empty($this->param)) {
            return false;
        }
        $obj = $this->filterField($this->param)->beforeAction($this->param);
        if ($this->depth < 2) {
            $table_fields = $this->fieldList();
            if (in_array('only_no', $table_fields)) {
                $maxNo = $this->max('only_no');
                $this->param['only_no'] = $maxNo + 1;
            }
            $primary_id = $obj->insertGetId($this->param);
            if (empty($primary_id)) {
                $this->throw("Failed to add data");
            }
            $this->param = array_merge($this->param, [$this->pk => $primary_id]);
            $this->listenSort($this->param);
            $this->newData = $this->getDataInfo([$this->pk => $primary_id]);
            $this->listenAdd();

            return $primary_id;
        }
        $obj->insertAll($this->param);
        return 1;
    }

    /**
     * 更新数据
     * @param $param
     * @return string
     */
    public function editData()
    {
        if (empty($this->param)) {
            return false;
        }
        $this->depth > 1 && $this->throw('Update parameter error', 8050);
        // 验证数据数组中是否包含主键ID
        !isset($this->param[$this->pk()]) && $this->throw('Primary key cannot be empty', 8051);
        $where[$this->pk()] = $this->param[$this->pk()];

        $this->oldData = $this->getDataInfo($where);
        $this->filterField($this->param)->beforeAction($this->param);
        $ret = $this->allowField($this->allow_filed_key ?? [])->withTrashed()->alias($this->alias)->where($where)->update($this->param); //返回更新成功影响的条数
        if ($ret === false) {
            $this->throw("Update failed");
        }
        $this->newData = $this->getDataInfo($where);

        $this->listenSort($this->param);
        $this->listenChildren($this->param);
        return $ret;
    }

    public function editDeletedData()
    {
        if (empty($this->param)) {
            return false;
        }
        $this->depth > 1 && $this->throw('Update parameter error', 8050);
        // 验证数据数组中是否包含主键ID
        !isset($this->param[$this->pk()]) && $this->throw('Primary key cannot be empty', 8051);
        $where[$this->pk()] = $this->param[$this->pk()];

        $this->oldData = $this->getDeletedDataInfo($where);
        $this->filterField($this->param)->beforeAction($this->param);
        $ret = $this->allowField($this->allow_filed_key ?? [])->withTrashed()->alias($this->alias)->where($where)->update($this->param); //返回更新成功影响的条数
        if ($ret === false) {
            $this->throw("Update failed");
        }
        $this->newData = $this->getDataInfo($where);

        $this->listenSort($this->param);
        $this->backParentData($this->newData);
        return $ret;
    }

    public function backParentData($param)
    {
        $table_fields = $this->fieldList();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields) || !isset($param[$this->pk])) {
            return true;
        }
        $allParent = $this->field($this->pk . ",children_ids")->whereFindInSet("children_ids", $param['parent_id'])->select();
        if (empty($allParent)) {
            return false;
        }
        foreach ($allParent as $k => $value) {
            $editMultiData[$k][$this->pk] = $value[$this->pk];
            $editMultiData[$k]['children_ids'] = $value['children_ids'] . $param['children_ids'] . ",";
        }
        $this->batchEditData($editMultiData, 'children_ids');
        return true;
    }


    /**
     * 删除数据
     * @param $param
     * @return int|array|bool
     */
    public function deleteData()
    {
        if (empty($this->param)) {
            return false;
        }
        $table_fields = $this->fieldList();
        if (!empty($this->param)) {
            $where = $this->filterSetWhere($this->param, false);
        }
        if (empty($where)) {
            return false;
        }
        $table_fields = $this->fieldList();
        $check = in_array("parent_id", $table_fields) && in_array("children_ids", $table_fields);
        if ($check) {
            $this->oldData = $this->getDataInfo($this->param);
        }

        if (in_array("deleted_at", $table_fields)) {
            $data = ['deleted_at' => time()];
            $this->where($where)->update($data);
        } else {
            $this->where($where)->delete();
        }
        if ($check) {
            $this->listenDel();
        }
        return true;
    }

    public function completelyDeleteData()
    {
        if (empty($this->param)) {
            return false;
        }
        $table_fields = $this->fieldList();
        if (!empty($this->param)) {
            $where = $this->filterSetWhere($this->param, false);
        }
        if (empty($where)) {
            return false;
        }
        $table_fields = $this->fieldList();
        $check = in_array("parent_id", $table_fields) && in_array("children_ids", $table_fields);
        if ($check) {
            $this->oldData = $this->getDataInfo($this->param);
        }
        $this->withTrashed()->where($where)->delete();
        if ($check) {
            $this->listenCompletelyDel();
        }
        return true;
    }

    /**
     * 监听子级维护子级数据
     *
     * @param array $param
     * @return bool
     */
    protected function listenChildren($param = [])
    {
        $table_fields = $this->fieldList();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields) || !isset($param[$this->pk])) {
            return true;
        }
        if ($this->newData['parent_id'] == $this->oldData['parent_id']) {
            return true;
        }
        $oldParent = $this->getDataInfo([$this->pk => $this->oldData['parent_id']]); //父级迁移 旧父级
        $newParent = $this->getDataInfo([$this->pk => $this->newData['parent_id']]); //新父级
        $arrChildren = empty($this->oldData['children_ids']) ? [] : array_filter(array_unique(explode(',', $this->oldData['children_ids'])));
        $children_ids = $this->oldData[$this->pk] . ",";
        $oldChildrenIds = [$this->newData['parent_id'], $this->oldData[$this->pk]];

        //找出子级集合中用更新的id---找旧的
        $oldList = $this->where($this->pk, "not in", $oldChildrenIds)
            ->whereFindInSet("children_ids", $this->oldData[$this->pk])
            ->field($this->pk)
            ->select();
        $oldIds = !empty($oldList) ? array_column($oldList->toArray(), $this->pk) : [];

        foreach ($arrChildren as $id) {
            if (empty($id)) {
                continue;
            }
            if (!empty($oldParent) && !empty($oldIds)) {
                $oldChildrenIds[] = $id;
                //把旧的子级集合里的id清掉
                $this->where($this->pk, "in", $oldIds)
                    ->update([
                        'children_ids' => Db::raw("replace(`children_ids`,',{$id},',',')"),
                    ]);
            }
            if (!empty($newParent)) {
                $children_ids .= $id . ",";
            }
        }
        if (!empty($newParent)) {
            $children_ids = implode(",", array_filter(array_unique(explode(",", $children_ids)))) . ",";
            $this->whereFindInSet("children_ids", $this->newData['parent_id'])->update([
                'children_ids' => Db::raw("concat(`children_ids`,'{$children_ids}')")
            ]);
        }
        return true;
    }

    /**
     * 添加监听
     *
     * @return bool
     */
    protected function listenAdd()
    {
        $table_fields = $this->fieldList();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        $pidField = empty($this->pidField) ? $this->pk : $this->pidField;
        //更新自身的子级
        $param["children_ids"] = "," . $this->newData[$pidField] . ",";
        $this->where([$pidField => $this->newData[$pidField]])->update($param);
        if (empty($this->newData['parent_id'])) {
            return true;
        }
        //更新父级子级
        $parent = $this->getDataInfo([$pidField => $this->newData['parent_id']]);
        if (empty($parent)) {
            $param['parent_id'] = 0; //父级不存在,维护数据
            $this->where([$pidField => $this->newData[$pidField]])->update($param);
            return true;
        }
        $parentParam['children_ids'] = $parent['children_ids'] . $this->newData[$pidField] . ",";
        $this->where([$pidField => $this->newData['parent_id']])->update($parentParam);

        $multiParent = $this->whereFindInSet("children_ids", $this->newData['parent_id'])
            ->where($pidField, "<>", $this->newData['parent_id'])
            ->select()->toArray();

        if (!empty($multiParent)) {
            $editMultiData = [];
            foreach ($multiParent as $k => $value) {
                $editMultiData[$k][$this->pk] = $value[$this->pk];
                $editMultiData[$k]['children_ids'] = $value['children_ids'] . $this->newData[$pidField] . ",";
            }
            $this->batchEditData($editMultiData, 'children_ids');
        }
        return true;
    }

    /**
     * 批量更新
     * $param = [
     *  [
     *      id=>1,
     *     field=>5,//更新的字段
     *  ]
     * ]
     * $updateField 更新的字段名
     * @param [type] $param
     * @return bool
     */
    public function batchEditData($param = [], $updateField = '')
    {
        if (empty($param) || empty($updateField)) {
            return false;
        }
        $ids = array_column($param, $this->pk);
        $fieldSql = " CASE " . $this->pk . " %s END";
        $str = " WHEN %s THEN '%s' ";
        $when_str = '';
        foreach ($param as $val) {
            if (!isset($val[$this->pk]) || !isset($val[$updateField])) {
                $this->throw("Batch update failed");
            }
            $when_str .= sprintf($str, $val[$this->pk], $val[$updateField]);
        }
        $updateData = sprintf($fieldSql, $when_str);
        $this->where([$this->pk => $ids])->update([$updateField => Db::raw($updateData)]);
        return true;
    }

    /**
     * 获取最大排序
     */
    public function getMaxSort()
    {
        return $this->max("sort");
    }

    /**
     * 监听排序
     *
     * @param array $param
     * @return bool
     */
    protected function listenSort($param = [])
    {
        $table_fields = $this->fieldList();
        if (!in_array("sort", $table_fields) || !isset($param['sort']) || !isset($param[$this->pk])) {
            return true;
        }
        //更新大于等于我的排序
        $this->where("sort", ">=", $param['sort'])->where($this->pk, "<>", $param[$this->pk])->update(['sort' => Db::raw("sort+1")]);
        return true;
    }

    /**
     * 监听删除-连子级一起删,父级数据维护
     *
     * @return bool
     */
    protected function listenDel()
    {
        $table_fields = $this->fieldList();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        $oldParent = $this->whereFindInSet("children_ids", $this->oldData[$this->pk])->where($this->pk, "<>", $this->oldData[$this->pk])->select()->toArray(); //父级迁移 旧父级

        $arrChildren = array_filter(array_unique(explode(",", $this->oldData['children_ids'])));

        $editParent = [];
        foreach ($arrChildren as $id) {
            if (empty($id)) {
                continue;
            }
            foreach ($oldParent as $parent) {
                if (!empty($parent)) {
                    $editParent[$parent[$this->pk]][$this->pk] = $parent[$this->pk];
                    $children_ids = empty($editParent[$parent[$this->pk]]['children_ids']) ? $parent['children_ids'] : $editParent[$parent[$this->pk]]['children_ids'];
                    $editParent[$parent[$this->pk]]['children_ids'] = str_replace("," . $id, "", $children_ids);
                }
            }
        }
        if (!empty($editParent)) { //全部父级更新子级
            $this->batchEditData($editParent, 'children_ids');
        }
        $where[$this->pk] = $arrChildren; //全部子级一起删
        if (in_array("deleted_at", $table_fields)) {
            $this->where($where)->update(['deleted_at' => time()]);
        } else {
            $this->where($where)->delete();
        }
        return true;
    }

    /**
     * 监听硬删除-连子级一起删,父级数据维护
     *
     * @return bool
     */
    protected function listenCompletelyDel()
    {
        $table_fields = $this->fieldList();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        $oldParent = $this->whereFindInSet("children_ids", $this->oldData[$this->pk])->where($this->pk, "<>", $this->oldData[$this->pk])->select()->toArray(); //父级迁移 旧父级

        $arrChildren = array_filter(array_unique(explode(",", $this->oldData['children_ids'])));

        $editParent = [];
        foreach ($arrChildren as $id) {
            if (empty($id)) {
                continue;
            }
            foreach ($oldParent as $parent) {
                if (!empty($parent)) {
                    $editParent[$parent[$this->pk]][$this->pk] = $parent[$this->pk];
                    $children_ids = empty($editParent[$parent[$this->pk]]['children_ids']) ? $parent['children_ids'] : $editParent[$parent[$this->pk]]['children_ids'];
                    $editParent[$parent[$this->pk]]['children_ids'] = str_replace("," . $id, "", $children_ids);
                }
            }
        }
        if (!empty($editParent)) { //全部父级更新子级
            $this->batchEditData($editParent, 'children_ids');
        }
        $where[$this->pk] = $arrChildren; //全部子级一起删
        $this->withTrashed()->where($where)->delete();
        return true;
    }

    /**
     * 过滤非模型的字段
     *
     * @param array $param
     * @return object
     */
    protected function filterField(&$param = [])
    {
        if (empty($param)) {
            return $this;
        }
        $table_fields = $this->fieldList();
        if ($this->depth < 2) {
            foreach ($param as $field => $value) {
                if (!in_array($field, $table_fields)) {
                    unset($param[$field]);
                }
            }
        } else {
            foreach ($param as $key => $arr) {
                if (is_array($arr)) {
                    foreach ($arr as $field => $value) {
                        if (!in_array($field, $table_fields)) {
                            unset($param[$key][$field]);
                        }
                    }
                }
            }
        }

        $this->setAddCommonParam($param); //设置入库的公共参数
        return $this;
    }

    /**
     * 设置入库的公共参数
     */
    public function setAddCommonParam(&$param)
    {
        return $this;
    }

    /**
     * 数据操作-前置方法
     */
    public function beforeAction(&$param)
    {
        return $this;
    }

    /**
     * 获取数据表的字段
     *
     * @return array
     */
    public function fieldList()
    {
        if (!empty($this->fieldList)) {
            return $this->fieldList;
        }
        $this->fieldList = $this->getTableFields();
        return $this->fieldList;
    }
}
