<?php

/**
 * 门面实体 
 */

namespace lhq\workflow\service\common;

use think\facade\Db;

/**
 * @method static array|null _getInfo() 主键查询 getInfo
 * @method static array|null _getDataInfo() 查询信息 getDataInfo
 * @method static array|null _getPageList() 分页列表 getPageList
 * @method static array|null _getDataList() 无分页列表 getDataList
 * @method static string|null _transHook() 事务钩子 transHook
 * @method static string|null _addMultiData() 联表添加数据 addMultiData
 * @method static string|null _addData() 添加 addData
 * @method static string|null _editData() 更新数据 editData
 * @method static string|null _deleteData() 删除数据-按主键删除 deleteData
 * @method static string|null _listenRedis() 监听- 删除旧缓存 listenRedis
 * @method static object _getClass() 获取对象 getClass
 * @method static object _model() 获取当前模型 model
 * @method static object|string|null _validateData() 检验参数 validateData
 * @method static object|string|null _setFilterWhere() 自定义过滤字段 setFilterWhere
 * @method static object|string|null _setFieldParam() 添加/编辑添加字段 setFieldParam
 * @method static object|string|null _setParam() 设置参数 setParam
 * @method static string|null _getPlatAlias() 获取系统别名 platAlias
 * @method static string|null _getMaxSort() 获取最大的排序 getMaxSort
 * 
 */
trait FacadeTrait
{

    use RedisTrait;

    protected static $initObj;

    /**
     * 门面
     *
     * @param string $method
     * @param array $avg
     * @return object|void
     */
    public static function __callStatic($method = '', $avg = [])
    {
        if (strpos("$" . $method, "_") == 1) {
            $objStatic = self::callInitObj();
            $method = str_replace("_", "", $method);
            if (method_exists($objStatic, $method)) {
                //插入redis穿透
                if (strtolower(substr($method, 0, 3)) == 'get' && !isset($objStatic->isNotRedis)) {
                    if (!isset($objStatic->redisHook[$objStatic->createHookKey($method, $avg)])) {
                        return $objStatic->redisHook($method, $avg);
                    }
                }
                return call_user_func_array([$objStatic, $method], $avg);
            }
        }
        throw new \Exception(__CLASS__ . " not function '" . $method . "'");
    }

    /**
     * 单例
     *
     * @param string $uuid
     * @param array $constructParam 构造函数参数
     * @return object
     */
    protected static function callInitObj($uuid = '0')
    {
        if (!empty(static::$initObj[$uuid])) {
            return static::$initObj[$uuid];
        }
        static::$initObj[$uuid] = static::classObject();
        return static::$initObj[$uuid];
    }

    /**
     * 获取对象.默认当前对象
     */
    protected static function classObject()
    {
        return new static();
    }
}
