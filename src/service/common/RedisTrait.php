<?php

/**
 * 
 * @method  array|null redisHook($action, $param, $default = false) 获取数据,经过redis，穿透方法
 * @method  array|null rPush($value) 右边入队列
 * @method  array|null blPop() 左边出队列
 * @method  array|null llen() 检查队列长度
 * @method  array|null setRedisKey($redisKey) 设置redis的key
 * @method  array|null setRedisTime($redisTime = 0) 设置缓存时间 默认：当天0时前有效
 * @method  array|null getRedisValue() 读取字符串型的redis
 * @method  array|null setRedisValue($value = null)保存数组型的redis
 * @method  array|null delRedis($key = '') 删缓存key 键
 * 
 */
namespace lhq\workflow\service\common;

use think\facade\Cache;
use think\facade\Db;

/**
 * redis 通用
 *
 * CREATE TABLE `d_redis` (
 * `id` int(11) NOT NULL AUTO_INCREMENT,
 * `redis_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'redis的key',
 * `redis_time` int(11) NOT NULL DEFAULT '0' COMMENT 'redis的缓存时长,单位秒',
 * `select_db` tinyint(5) NOT NULL COMMENT 'redis所在数据库0-15',
 * `remark` varchar(255) NOT NULL DEFAULT '' COMMENT 'redis的key的业务备注',
 * `is_cache` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开启缓存 : 默认开启',
 * PRIMARY KEY (`id`) USING BTREE,
 * UNIQUE KEY `redis_key` (`redis_key`) USING BTREE,
 * KEY `select_db` (`select_db`) USING BTREE,
 * KEY `redis_time` (`redis_time`) USING BTREE
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
trait RedisTrait
{

    protected $redisHook = []; //redis钩子
    protected $redisKey = null; //键前缀
    protected $redisTime = null; //缓存时间
    protected $defaultLongTime = 60 * 30; //默认时长
    protected $redisKeyName = null; //键名
    protected $redisDb = 0; //缓存库编号
    protected $isCache = true; //是否穿透
    protected $objRedis = []; //redis服务对象
    protected $isDbConfig = true; //是否数据库配置缓存

    /**
     * 获取redis对象
     *
     * @return object
     */
    protected function getObjRedis()
    {
        return $this->selectRedisDb();
    }

    /**
     * 选择redis数据库
     *
     * @return object
     */
    protected function selectRedisDb()
    {
        $redisDb = $this->redisDb;
        if (!empty($this->objRedis[$this->getClassName()])) {
            return $this->objRedis[$this->getClassName()];
        }
        //获取数据库配置
        // $config = $this->redisConfig();
        $config = [];
        if (!empty($config)) { //配置存在 优先数据库配置 > 代码配置 > 默认配置
            $redisDb = $config['select_db'];
            $this->isCache = $config['is_cache'] ? true : false;
            $this->redisTime = empty($config['redis_time']) ? $this->redisTime : $config['redis_time'];
        }
        $this->objRedis[$this->getClassName()] = Cache::store("redis")->handler();
        $this->objRedis[$this->getClassName()]->select($redisDb);

        return $this->objRedis[$this->getClassName()];
    }

    /**
     * 获取redis的配置
     */
    protected function redisConfig()
    {
        if (!$this->isDbConfig) {
            return [];
        }
        $objRedis = Cache::store("redis")->handler();
        $redisKey = "RedisToolTrait:" . md5("redisConfig");
        $ret = $objRedis->get($redisKey);
        if (!empty($ret)) {
            return json_decode($ret, true);
        }
        $info = Db::table("d_redis333333333")->where(['redis_key' => $this->getClassName()])->find();
        if (empty($info)) {
            return [];
        }
        $objRedis->setex($redisKey, 60 * 60 * 24 * 7, json_encode($info, JSON_UNESCAPED_UNICODE));
        return $info;
    }

    /**
     * 生成钩子key
     *
     * @param [type] $action
     * @param [type] $param
     * @return string
     */
    public function createHookKey($action, $param)
    {
        return $this->md5Key(func_get_args());
    }

    /**
     * 获取数据,经过redis
     * @param string $action 访问方法
     * @param array $param 参数 数组/字符串
     * @param false $default 是否穿透缓存 true / false
     * func_get_args() == [$param]
     * 使用方法
     *  if (!isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
     *      return $this->redisHook(__FUNCTION__, func_get_args());
     *  }
     * @return mixed
     */
    public function redisHook($action, $param, $default = false)
    {
        if (!method_exists($this, $action)) {
            throw new \Exception("method does not exist", 9404);
        }
        $this->selectRedisDb();
        $this->redisHook[$this->createHookKey($action, $param)] = 1; //记录访问次数
        if ($default || !$this->isCache) { // 是否穿透 || 是否缓存
            $this->delRedis();
            return call_user_func_array([$this, $action], $param);
        }
        $redisName = $this->getClassName() . $action . ':' . $this->md5Key(func_get_args());
        $ret = $this->setRedisKey($redisName)->getRedisValue();
        if (!empty($ret) && !$default) {
            return $ret;
        }
        $data = call_user_func_array([$this, $action], $param);
        if (is_bool($data)) {
            return $data;
        }
        $this->setRedisKey($redisName)->setRedisValue($data);
        return $data;
    }

    /**
     * 右边入队列
     */
    public function rPush($value)
    {
        return $this->getObjRedis()->rPush($this->getRedisKayName(), $value);
    }

    /**
     * 左边出队列
     */
    public function blPop()
    {
        if (empty($this->llen())) {
            return null;
        }
        $i = 0;
        while (true) {
            //从右边（rPush）入队，左边阻塞出队
            $data = $this->getObjRedis()->blPop($this->getRedisKayName(), 10);
            if ($data) {
                return $data;
            } else {
                sleep(1);
                ++$i;
            }
            if ($i >= 10) { //十秒超时
                throw new \Exception("time out", 9502);
            }
        }
    }

    /**
     * 检查队列长度
     */
    public function llen()
    {
        return $this->getObjRedis()->llen($this->getRedisKayName());
    }

    /**
     * Md5 加密一下key
     */
    public function md5Key($param, $default = false)
    {
        if (empty($param)) {
            return '';
        }
        if (is_array($param)) {
            return md5(json_encode($param));
        } else {
            if ($default === true) {
                return md5($param);
            }
            return $param;
        }
    }

    /**
     * 获取当前对象名-做redis前缀
     *
     * @return string
     */
    protected function getClassName()
    {
        if (!empty($this->redisKey)) {
            $arr = explode(":", $this->redisKey);
            if (!empty($arr)) {
                return $arr[0] . ":";
            }
        }
        $arr = explode("\\", get_class());
        $name = end($arr);
        if (!empty($name)) {
            return $name . ":";
        }
        return 'RedisToolTrait:';
    }

    /**
     * 获取redis键名
     *
     * @return string
     */
    protected function getRedisKayName()
    {
        if (empty($this->redisKeyName)) {
            $this->redisKeyName = $this->getClassName();
        }
        return $this->redisKeyName;
    }

    /**
     * 设置redis的key
     */
    public function setRedisKey($redisKey)
    {
        $this->redisKeyName = $redisKey;
        return $this;
    }

    /**
     * 设置缓存时间
     * 默认：当天0时前有效
     */
    public function setRedisTime($redisTime = 0)
    {
        if (is_numeric($redisTime) && !empty($redisTime)) {
            $this->redisTime = $redisTime;
        }
        if (empty($this->redisTime)) {
            $this->redisTime = strtotime(date("Y-m-d 23:59:39")) - time();
        }
        return $this;
    }

    /**
     * 读取字符串型的redis
     */
    public function getRedisValue()
    {
        $data = $this->getObjRedis()->get($this->getRedisKayName());
        if (!empty($data)) {
            $arr = json_decode($data, true);
            if (empty($arr)) {
                return $data;
            }
            return $arr;
        }
        return null;
    }

    /**
     * 保存数组型的redis
     */
    public function setRedisValue($value = null)
    {
        if (empty($value)) {
            return true;
        }
        $this->setRedisTime($this->defaultLongTime ?? 0); //设置缓存时间。默认当天0点失效
        if (empty($this->redisTime)) {
            return false;
        }
        if (is_array($value)) {
            $saveValue = json_encode($value, JSON_UNESCAPED_UNICODE);
        } else {
            $saveValue = $value;
        }
        return $this->getObjRedis()->setex($this->getRedisKayName(), $this->redisTime, $saveValue);
    }

    /**
     * 删缓存
     *
     * @return bool
     */
    public function delRedis($string = '')
    {
        if (!empty($string)) {
            $keys = $this->getObjRedis()->keys($string);
            if (!empty($keys)) {
                $this->getObjRedis()->del($keys);
            }
            return true;
        }
        $keyName = $this->getRedisKayName();
        if (strchr($keyName, ":")) {
            $redisKey = explode(":", $keyName)[0] . ":";
        } else {
            $redisKey = $keyName;
        }
        $keys = $this->getObjRedis()->keys($redisKey . '*');
        if (!empty($keys)) {
            $this->getObjRedis()->del($keys);
        }
        return true;
    }
}
