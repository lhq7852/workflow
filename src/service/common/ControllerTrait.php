<?php

namespace lhq\workflow\service\common;

use think\facade\View;

trait ControllerTrait
{
    use UtilTrait;

    protected function workflowView($path, $assign)
    {
        View::config(['view_path' => dirname(dirname(__DIR__)) . '/view/']);
        return View::fetch($path, $assign);
    }
}