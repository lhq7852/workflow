<?php

/**
 * 验证器实体
 * @method  array|null sceneEditData() 编辑不检查必填
 * @method  array|null getClass() 获取服务对象
 * @method  array|null checkBeing($value, $rule, $data = []) 唯一检查
 * @method  array|null checkJson($value, $rule, $data = [])检查是否json格式
 * @method  array|null checkImageSize($value, $rule, $data = [])检查图片大小
 */

namespace lhq\workflow\service\common;

trait ValidateTrait
{

    // protected $being = "alias|title"; //唯一字段检查

    /**
     * 编辑不检查必填
     *
     * @return object
     */
    public function sceneEditData()
    {
        if (!empty($this->rule)) {
            foreach ($this->rule as $field => $rule) {
                $this->remove($field, 'require'); //编辑的时候移除全部规则的必填
            }
        }
        return $this; //编辑默认 不验证参数
    }

    /**
     * 获取服务对象
     *
     * @return string|object
     */
    protected function getClass()
    {
        $file = explode('\\', get_class());
        $class_name = array_pop($file);
        $path = array_pop($file);
        return implode("\\", $file) . '\\' . str_replace("Validate", "", $class_name) . "Service";
    }

    /**
     * 唯一检查
     *
     * @param [type] $value
     * @param [type] $rule
     * @param array $data
     * @return string|bool
     */
    protected function checkBeing($value, $rule, $data = [])
    {
        if (empty($this->being)) {
            return "请配置being属性";
        }
        $class = $this->getClass();
        $model = $class::_model();
        $pk = $model->pk();
        $arr = explode("|", $this->being);
        foreach ($arr as $being) {
            if (strchr($being, "+")) {
                $a = explode("+", $being);
                $param = [];
                foreach ($a as $v) {
                    $param[$v] = $data[$v];
                }
                $being = $a[0] ?? $v;
                $check = $class::_getDataInfo($param, false);
            } else {
                $check = $class::_getDataInfo([$being => $value], false);
            }
            if (!empty($check) && empty($data[$pk])) {
                return "[" . $being . "]已存在,请重新输入";
            }
            if (!empty($check) && !empty($data[$pk]) && $data[$pk] != $check[$pk]) {
                return "[" . $being . "]已存在,请重新输入";
            }
        }
        return true;
    }

    /**
     * 检查是否json格式
     */
    protected function checkJson($value, $rule, $data = [])
    {
        if (empty($value)) {
            return lang(811) ?? false;
        }
        $arrValue = json_decode($value, true);
        if (empty($arrValue)) {
            return lang(811) ?? false;
        }
        return true;
    }

    /**
     * 检查图片大小
     * rule : 2048KB,字段
     */
    protected function checkImageSize($value, $rule, $data = [])
    {
        if (strchr($rule, ",")) {
            $arr = explode(",", $rule);
            $size_kb = $arr[0] ?? 0;
            $field = $arr[1] ?? '';
        } else {
            $size_kb = $rule;
            $field = '';
        }
        $arrValue = json_decode($value, true);
        if (!empty($arrValue) && is_array($arrValue) && !empty($field)) {
            foreach ($arrValue as $v) {
                if (!empty($v[$field])) {
                    $size = $this->getFileSize($v[$field]);
                    if ($size >= $size_kb * 1024) {
                        return lang(813) ?? false;
                    }
                }
            }
            return true;
        }
        $size = $this->getFileSize($value);
        if ($size >= $size_kb * 1024) {
            return lang(813) ?? false;
        }
        return true;
    }
}
