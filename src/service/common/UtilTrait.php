<?php

namespace lhq\workflow\service\common;

trait UtilTrait
{
    use ExceptionTrait;

    /**
     * 检查数组的维数: 一维 或者 二位[以上]
     * @param array|string $array
     * @return int
     */
    protected function checkArrayDepth($array)
    {
        if (!is_array($array)) {
            return 0;
        }

        $i = 0;
        foreach ($array as $item) {
            if (is_array($item)) {
                $i = 2;
                break;
            }
            $i = 1;
        }
        return $i;
    }

    /**
     * api接口-成功返回
     */
    protected function success($data = [], $default = true)
    {
        return json([
            'code' => 200,
            'msg' => '成功',
            'data' => $data,
            'request_no' => request()->request_no ?? date("YmdHis") . rand(100, 999),
        ]);
    }

    /**
     * api接口-失败返回
     */
    protected function error(\Exception $e)
    {
        return json([
            'code' => $e->getCode() ?: 500,
            'msg' => $e->getMessage(),
            'request_no' => request()->request_no ?? date("YmdHis") . rand(100, 999),
            'msg_str' => env("APP_DEBUG") ? explode("\n", $e->getTraceAsString()) : '',
        ]);
    }

    /**
     * @param $delimiter 分割符
     * @param $str 要分割的字符串
     * @return array|false|string[]
     */
    protected function getExplode($delimiter, $str)
    {
        if (!strlen($str)) {
            return array();
        }

        return explode($delimiter, $str);
    }
}