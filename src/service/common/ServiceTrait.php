<?php

/**
 * 服务实体
 */

namespace lhq\workflow\service\common;

use think\exception\ValidateException;
use think\facade\Db;

/**
 * @method static array|null _getInfo() 主键查询 getInfo
 * @method static array|null _getDataInfo() 查询信息 getDataInfo
 * @method static array|null _getDeletedDataInfo() 查询已软删信息 getDeletedDataInfo
 * @method static array|null _getPageList() 分页列表 getPageList
 * @method static array|null _getPageDeletedList() 软删分页列表 getPageDeletedList
 * @method static array|null _getDataDeletedList() 软删无分页列表 getDataDeletedList
 * @method static array|null _getDataList() 无分页列表 getDataList
 * @method static string|null _transHook() 事务钩子 transHook
 * @method static string|null _addMultiData() 联表添加数据 addMultiData
 * @method static string|null _addData() 添加 addData
 * @method static string|null _editData() 更新数据 editData
 * @method static string|null _recoveryData() 恢复数据 recoveryData
 * @method static string|null _deleteData() 删除数据-按主键删除 deleteData
 * @method static string|null _completelyDeleteData() 硬删除数据-按主键删除 completelyDeleteData
 * @method static string|null _listenRedis() 监听- 删除旧缓存 listenRedis
 * @method static object _getClass() 获取对象 getClass
 * @method static object _model() 获取当前模型 model
 * @method static object|string|null _validateData() 检验参数 validateData
 * @method static object|string|null _setFilterWhere() 自定义过滤字段 setFilterWhere
 * @method static object|string|null _setFieldParam() 添加/编辑添加字段 setFieldParam
 * @method static object|string|null _setParam() 设置参数 setParam
 * @method static string|null _getPlatAlias() 获取系统别名 platAlias
 * @method static string|null _getMaxSort() 获取最大的排序 getMaxSort
 *
 */
trait ServiceTrait
{

    use  UtilTrait;

    protected $newData = []; //新增的数据
    protected $oldData = []; //更新前旧数据
    protected $oldDataList = []; //更新前旧数据
    protected $param = []; //传入参数
    protected $isValidate = true; //默认检查
    protected $isTrans = true; //默认开启事务
    protected $isEdit = false; //有才更新

    /**
     * 设置传入的参数
     *
     * @param [type] $param
     * @return object
     */
    public function setParam($param = [])
    {
        $this->param = $param;
        return $this;
    }

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    public function setFieldParam(&$param = [], $mode = "add")
    {
        return $this;
    }

    /**
     * 自定义过滤字段
     *
     * @param array $param
     * @param [type] $method
     * @return object
     */
    protected function setFilterWhere(&$param = [], $method = null)
    {
        return $this;
    }

    /**
     * 检验参数
     * validate
     * @param array $param
     * @param string $action
     * @return object
     */
    public function validateData($param = [], $action = '')
    {
        if (!$this->isValidate) {
            return $this;
        }

        //验证逻辑
        try {
            $this->getClass('validate', false);
            if (class_exists($this->classFile) && is_array($param)) {
                validate($this->classFile)->scene($action)->check($param);
            }
        } catch (ValidateException $e) {
            $this->throw($this->content_replace($e->getError(), $this->model()->fieldDict()->fieldDict));
        }
        return $this;
    }

    //==================================================================================================

    /**
     * 获取当前模型
     *
     * @return object
     */
    public function model($default = true)
    {
        if (!empty($this->model)) {
            return $this->model;
        }
        $this->model = $this->getClass('model');
        if (empty($this->model)) {
            if (!$default) {
                return null;
            }
            $this->throw("can't find {$this->classFile}", 6001);
        }
        return $this->model;
    }

    /**
     * 获取对象
     *
     * @param [type] $string
     * @return object|null|string
     */
    protected function getClass($string = '', $default = true)
    {
        $file = $this->getExplode('\\', get_class());
        $this->className = array_pop($file);
        $file[] = $string;
        $file[] = $this->className . ucfirst($string);
        $this->classFile = implode('\\', $file);
        if (class_exists($this->classFile) && $default) {
            return (new $this->classFile());
        }
        $this->classFile = str_replace("Service", "", $this->classFile);
        if (class_exists($this->classFile) && $default) {
            return (new $this->classFile());
        }
        return null;
    }

    /**
     * 查询-主键查询
     */
    public function getInfo($param)
    {
        if (empty($param[$this->model()->pk()])) {
            return [];
        }
        return $this->getDataInfo($param[$this->model()->pk()]);
    }

    /**
     * 查询
     */
    public function getDataInfo($param)
    {
        if (is_numeric($param)) {
            $this->newData = $this->model()->where([$this->model()->pk() => $param])->find();
            if (!empty($this->newData)) {
                return $this->newData->toArray();
            } else {
                return [];
            }
        }
        $this->newData = $this->model()->getDataInfo($param);
        return $this->newData;
    }

    /**
     * 查询已软删数据
     * @param $param
     * @return array
     */
    public function getDeletedDataInfo($param)
    {
        if (is_numeric($param)) {
            $this->newData = $this->model()->withTrashed()->where('deleted_at', '>', 0)->where([$this->model()->pk() => $param])->find();
            if (!empty($this->newData)) {
                return $this->newData->toArray();
            } else {
                return [];
            }
        }
        $this->newData = $this->model()->getDeletedDataInfo($param);
        return $this->newData;
    }

    /**
     * 分页列表
     *
     * @param array $param
     * @return array
     */
    public function getPageList($param = [])
    {
        $list = $this->setFilterWhere($param, __FUNCTION__)->model()->getPageList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 无分页列表
     *
     * @param array $param
     * @return array
     */
    public function getDataList($param = [])
    {
        $list = $this->setFilterWhere($param, __FUNCTION__)->model()->getDataList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 软删无分页列表
     *
     * @param array $param
     * @return array
     */
    public function getDataDeletedList($param = [])
    {
        $list = $this->setFilterWhere($param, __FUNCTION__)->model()->getDataDeletedList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 软删分页列表
     *
     * @param array $param
     * @return array
     */
    public function getPageDeletedList($param = [])
    {
        $list = $this->setFilterWhere($param, __FUNCTION__)->model()->getPageDeletedList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 事务钩子
     *
     * @param [string] $action
     * @param array $param
     * @param boolean $default
     * @return array|string|Exception
     * if (!isset($this->transHook[__FUNCTION__])) {
     *      return $this->transHook(__FUNCTION__, func_get_args());
     *  }
     */
    public function transHook($action = '', $param = [])
    {
        if (!method_exists($this, $action)) {
            $this->throw("Method not found");
        }
        if (!empty($this->transHook[$action])) {
            return call_user_func_array([$this, $action], $param);
        }
        $this->transHook[$action] = 1; //记录访问次数
        Db::startTrans();
        try {
            $result = call_user_func_array([$this, $action], $param);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            $this->throw($e->getMessage(), 7001);
        }
        return $result;
    }

    /**
     * 联表添加数据-多表
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function addMultiData(array $param = [])
    {
        $param = $this->param = array_merge($param, $this->param);
        if (empty($param['service'])) {
            $this->throw("No service", 8001);
        }
        $arrService = $param['service'];
        unset($param['service']);
        unset($this->param['service']);
        $this->isTrans = false; //防止重复事务
        Db::startTrans();
        try {
            $id = 0;
            //全部服务都先参数验证
            foreach ($arrService as $service) {
                $service::_setParam($this->param)->setFieldParam($this->param, 'add')->validateData($this->param, "addData");
            }
            //从第一个服务开始一个个添加数据
            foreach ($arrService as $k => $service) {
                $model = $service::_setParam($this->param)->setFieldParam($this->param, 'add')->model();
                $this->param[$model->pk()] = $model->setParam($this->param)->addData();
                if ($k == 0) {
                    $id = $this->param[$model->pk()];
                }
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            $this->throw($e->getMessage() . $e->getFile() . $e->getLine(), 7001);
        }
        return $id;
    }


    /**
     * 新增数据-单表
     * 设计流程：   设置数据-检查数据-插入库-监听
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function addData(array $param = [])
    {
        $param = array_merge($param, $this->param);
        if ($this->isTrans && !isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        $id = $this->setFieldParam($param, 'add')->validateData($param, __FUNCTION__)->model()->setParam($param)->addData();

        $this->newData = $this->getDataInfo($id);
        $this->listenRedis(__FUNCTION__);
        return $id;
    }

    /**
     * 更新数据
     * @param $param
     * @return string|object
     */
    public function editData(array $param = [], $where = null)
    {
        $pk = $this->model()->pk();
        !isset($param[$pk]) && empty($where) && $this->throw('Parameter error', 8010);
        if (!empty($param[$pk])) {
            $where[$pk] = $param[$pk];
        }
        $this->oldData = $this->getDataInfo($where);
        empty($this->oldData) && $this->throw('Data does not exist', 8404);

        $param = array_merge($param, [$pk => $this->oldData[$pk]]);
        // 是否有数据不一致 有才更新 $isEdit
        foreach ($this->oldData as $key => $value) {
            if ($key == $pk) {
                continue;
            }
            if (isset($param[$key])) {
                if ($value != $param[$key]) {
                    $this->isEdit = true;
                }
            }
        }
        if (!$this->isEdit) {
            return true;
        }
        $this->setFieldParam($param, 'edit')->validateData($param, __FUNCTION__)->model()->setParam($param)->editData();
        $this->editParam = $param; //更新数据记录
        $this->newData = $this->getDataInfo($this->oldData[$pk]);
        $this->listenRedis(__FUNCTION__);

        return true;
    }

    /**
     * 根据主键删除单条/多条数据
     * @param $param primary_key|where  主键或者条件数组
     * @return mixed
     * @throws Exception
     */
    public function deleteData($param = [])
    {
        $pk = $this->model()->pk();
        !isset($param[$pk]) && $this->throw('Parameter error', 8010);
        Db::startTrans();
        try {
            $arrPk = is_array($param[$pk]) ? $param[$pk] : explode(",", $param[$pk]);
            $arrPk = array_unique($arrPk);
            $table_fields = $this->model()->fieldList();
            $this->oldDataList = $this->getDataList([$pk => $arrPk]);
            //存在子级，需要一个个删除
            if (in_array("parent_id", $table_fields) && in_array("children_ids", $table_fields)) {
                foreach ($arrPk as $id) {
                    $this->model()->setParam([$pk => $id])->deleteData();
                }
            } else {
                $this->model()->setParam([$pk => $arrPk])->deleteData();
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            $this->throw($e->getMessage() . $e->getFile() . $e->getLine(), 8071);
        }

        $this->listenRedis(__FUNCTION__);
        return true;
    }

    /**
     * 恢复数据
     * @param $param
     * @return string|object
     */
    public function recoveryData($param)
    {
        $pk = $this->model()->pk();
        !isset($param[$pk]) && empty($where) && $this->throw('Parameter error', 8010);
        if (!empty($param[$pk])) {
            $where[$pk] = $param[$pk];
        }
        $this->oldData = $this->getDeletedDataInfo($where);
        empty($this->oldData) && $this->throw('Data does not exist', 8404);

        $param['deleted_at'] = 0;
        $param = array_merge($param, [$pk => $this->oldData[$pk]]);
        // 是否有数据不一致 有才更新 $isEdit
        foreach ($this->oldData as $key => $value) {
            if ($key == $pk) {
                continue;
            }
            if (isset($param[$key])) {
                if ($value != $param[$key]) {
                    $this->isEdit = true;
                }
            }
        }
        if (!$this->isEdit) {
            return true;
        }
        $this->setFieldParam($param, 'recovery')->validateData($param, __FUNCTION__)->model()->setParam($param)->editDeletedData();
        $this->editParam = $param; //更新数据记录
        $this->newData = $this->getDataInfo($this->oldData[$pk]);

        $this->listenRedis(__FUNCTION__);
        return true;
    }

    /**
     * 根据主键硬删除单条/多条数据
     * @param $param primary_key|where  主键或者条件数组
     * @return mixed
     * @throws Exception
     */
    public function completelyDeleteData($param)
    {
        $pk = $this->model()->pk();
        !isset($param[$pk]) && $this->throw('Parameter error', 8010);
        Db::startTrans();
        try {
            $arrPk = is_array($param[$pk]) ? $param[$pk] : explode(",", $param[$pk]);
            $arrPk = array_unique($arrPk);
            $table_fields = $this->model()->fieldList();
            $this->oldDataList = $this->getDataDeletedList([$pk => $arrPk]);
            //存在子级，需要一个个删除
            if (in_array("parent_id", $table_fields) && in_array("children_ids", $table_fields)) {
                foreach ($arrPk as $id) {
                    $this->model()->setParam([$pk => $id])->completelyDeleteData();
                }
            } else {
                $this->model()->setParam([$pk => $arrPk])->completelyDeleteData();
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            $this->throw($e->getMessage() . $e->getFile() . $e->getLine(), 8071);
        }

        $this->listenRedis(__FUNCTION__);
        return true;
    }

    /**
     *  监听- 删除旧缓存 redis
     *
     * @return object
     */
    public function listenRedis($method = null)
    {
        //添加监听
        if (in_array($method, ['addData']) && method_exists($this, 'listenAdd')) {
            $this->listenAdd();
        }
        //编辑监听
        if (in_array($method, ['editData', 'batchEditData', 'recoveryData']) && method_exists($this, 'listenEdit')) {
            $this->listenEdit();
        }
        //删除监听
        if (in_array($method, ['deleteData']) && method_exists($this, 'listenDel')) {
            $this->listenDel();
        }
        //硬删监听
        if (in_array($method, ['completelyDeleteData']) && method_exists($this, 'listenCompletelyDel')) {
            $this->listenCompletelyDel();
        }
        //自动删除redis缓存
        if (method_exists($this, 'delRedis')) {
            $this->delRedis();
        }
        return $this;
    }

    /**
     * 获取系统别名-无缓存
     */
    public function platAlias()
    {
        if (!empty($this->current_plat_alias)) {
            return $this->current_plat_alias;
        }
        $host = $_SERVER['HTTP_HOST'];
        $config = array_flip(config("hlconfig.hosts"));
        $plat_alias = $this->current_plat_alias = $config[$host] ?? 'oa';
        return $plat_alias;
    }

    /**
     * 获取最大的排序
     */
    public function getMaxSort()
    {
        return (int)$this->model()->getMaxSort() + 1;
    }
}
