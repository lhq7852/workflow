<?php

/**
 * 异常实体
 */

namespace lhq\workflow\service\common;

trait ExceptionTrait
{

    /**
     * 抛异常
     * @param 
     *  msg:错误描述
     *  code : 错误码
     */
    protected function throw($msg = '', $code = 500)
    {
        $message = lang($msg ?: "Error Processing Request");
        throw new \Exception($message, $code ?: 500);
    }

    /**
     * 6001 : 找不到模型
     * 7001 : 数据事务
     * 8404 : 数据不存在
     */
}
